package com.nyangames.BlockRunner.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.nyangames.BlockRunner.BlockRunner;
import com.nyangames.BlockRunner.GLoop;

public class DesktopLauncher {
	public static void main (String[] arg) {
		new LwjglApplication(new BlockRunner(),"Block Runner", 1280, 720);
	}
}
