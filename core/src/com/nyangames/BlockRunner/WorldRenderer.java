package com.nyangames.BlockRunner;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by NyanD on 6/07/2014.
 */
public class WorldRenderer {
    private World world;
    private OrthographicCamera cam;

    private ShapeRenderer debugRenderer;

    public WorldRenderer(World world){
        debugRenderer = new ShapeRenderer();
        this.world = world;
        this.cam = new OrthographicCamera(10,7);
        this.cam.position.set(5,3.5f,0);
        this.cam.update();
    }

    public OrthographicCamera getCam(){
        return cam;
    }

    public void render(){
        debugRenderer.setProjectionMatrix(cam.combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (Block b : world.getBlocks()){
            Rectangle rect = b.getBounds();
            float x1 = b.getPosition().x + rect.x;
            float y1 = b.getPosition().y + rect.y;
            debugRenderer.setColor(new Color(1,0,0,1));
            debugRenderer.rect(x1,y1,rect.width,rect.height);
        }

        Player player = world.getPlayer();
        Rectangle rect = player.getBounds();
        float x1 = player.getPosition().x + rect.x;
        float y1 = player.getPosition().y + rect.y;
        debugRenderer.setColor(new Color(0,1,0,1));
        debugRenderer.rect(x1,y1,rect.width,rect.height);
        debugRenderer.end();
    }
}
