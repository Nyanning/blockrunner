package com.nyangames.BlockRunner;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by NyanD on 6/07/2014.
 */
public class Player {

    static final float SPEED = 2f, JUMPVELOCITY = 1f, SIZE = 0.5f;

    private Vector2 position, accel, velocity;
    private Rectangle bounds;
    private State state;
    private boolean faceLeft;

    public Player(float x, float y){
        position = new Vector2(x,y);
        accel = new Vector2(0,0);
        velocity = new Vector2(0,0);
        bounds = new Rectangle();

       bounds.setWidth(SIZE);
       bounds.setHeight(SIZE);

        state = State.IDLE;
        faceLeft = true;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }
}
