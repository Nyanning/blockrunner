/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NyanD on 11/07/2014.
 */
public class SpriteFactory {
    private SpriteBatch batch;
    private Map<String, Texture> textureMap;

    public SpriteFactory(SpriteBatch batch) {
        this.batch = batch;
        this.textureMap = new HashMap<String, Texture>();
    }

    public Sprite createSprite(String location){
        Texture spriteTexture;
        try {
            spriteTexture = getTexture(location);
        } catch (Exception e) {
            throw new RuntimeException("Image file at '" + location + "' could not be");
        }

        return new Sprite(spriteTexture);
    }

    public Animation createAnimation(String location, int xOffset, int yOffset, int width, int height, float animSpeed){
        Texture animationTexture;
        try {
            animationTexture = getTexture(location);
        } catch (Exception e){
            throw new RuntimeException("Image file at '"+location+"' could not be found");
        }

        TextureRegion[][] textureRegionsFrames = TextureRegion.split(animationTexture, width, height);

        int frameLength = textureRegionsFrames.length * textureRegionsFrames[0].length;
        TextureRegion[] frames = new TextureRegion[frameLength];

        int frameIndex = 0;
        for (int i=0;i<textureRegionsFrames.length;i++){
            for (int j=0;j<textureRegionsFrames[0].length;j++){
                frames[frameIndex++] = textureRegionsFrames[i][j];
            }
        }

        return new Animation(animSpeed,frames);
    }

    private Texture getTexture(String location) throws Exception{
        Texture texture;
        if (textureMap.containsKey(location)){
            texture = textureMap.get(location);
        } else {
            texture = new Texture(Gdx.files.internal(location));
            if (texture == null){
                throw new Exception("Image file at '"+location+"' could not be loaded");
            }
            textureMap.put(location, texture);
        }

        return texture;
    }
}
