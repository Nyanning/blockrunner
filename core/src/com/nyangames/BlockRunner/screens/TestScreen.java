
package com.nyangames.BlockRunner.screens;

import com.nyangames.BlockRunner.eos.entities.EntityFactory;
import com.nyangames.BlockRunner.eos.systems.SystemManager;

/**
 * Created by NyanD on 11/07/2014.
 */
public class TestScreen extends BaseScreen{
    public TestScreen(SystemManager systemManager, EntityFactory entityFactory) {
        super(systemManager, entityFactory);
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
