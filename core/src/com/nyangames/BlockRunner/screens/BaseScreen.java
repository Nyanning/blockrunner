package com.nyangames.BlockRunner.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.nyangames.BlockRunner.eos.entities.EntityFactory;
import com.nyangames.BlockRunner.eos.systems.SystemManager;

/**
 *
 */
public abstract class BaseScreen implements Screen {
    private SystemManager systemManager;
    private EntityFactory entityFactory;

    public BaseScreen(SystemManager systemManager, EntityFactory entityFactory) {
        this.systemManager = systemManager;
        this.entityFactory = entityFactory;
    }

    public BaseScreen(){
        systemManager = new SystemManager();
        entityFactory = new EntityFactory();
    }
    @Override
    public final void render(float delta) {
        update(delta);
    }

    /**
     * Update loop for the BaseScreen
     * @param delta
     */
    public abstract void update(float delta);

}
