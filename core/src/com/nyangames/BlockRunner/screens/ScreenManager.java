package com.nyangames.BlockRunner.screens;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages screens
 */
public class ScreenManager {
    private Map<String, BaseScreen> screens;

    public ScreenManager(){
        screens = new HashMap<String, BaseScreen>();
    }

    public void addScreen(String id, BaseScreen screen){
        if (screens.containsKey(id)){
            throw new RuntimeException("Screen with id '"+id+"' already exists");
        }

        screens.put(id, screen);
    }

    public void removeScreen(String id){
        screens.remove(id);
    }

    /**
     * Disposes all the screens
     */
    public void dispose(){
        for (BaseScreen screen : screens.values()){
            screen.dispose();
        }

        screens = new HashMap<String, BaseScreen>();
    }

    public BaseScreen getScreen(String id){
        if (!screens.containsKey(id)){
            throw new RuntimeException("Screen with id '"+id+"' does not exist");
        }

        return screens.get(id);
    }
}
