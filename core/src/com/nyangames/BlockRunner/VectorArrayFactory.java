/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner;

import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;
import java.util.List;

/**
 * Factory that generates an array of 2d vectors given set of (x,y) pairs
 */
public class VectorArrayFactory {

    public static Vector2[] create(float... values){
        if (values.length == 0){
            throw new RuntimeException("Values for the vectors are missing");
        }
        if (values.length % 2 == 1){
            throw new RuntimeException("Odd number of vector values");
        }
        List<Vector2> vectors = new LinkedList<Vector2>();
        for (int i=0;i<values.length;i+=2){
            vectors.add(new Vector2(values[i],values[i+1]));
        }

        return vectors.toArray(new Vector2[vectors.size()]);
    }
}
