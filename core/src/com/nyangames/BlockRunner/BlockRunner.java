package com.nyangames.BlockRunner;

import com.badlogic.gdx.Game;
import com.nyangames.BlockRunner.eos.entities.EntityFactory;
import com.nyangames.BlockRunner.eos.systems.PhysicsSystem;
import com.nyangames.BlockRunner.screens.ScreenManager;

/**
 * Created by NyanD on 6/07/2014.
 */
public class BlockRunner extends Game {
    private ScreenManager screenManager;
    @Override
    public void create() {
        screenManager = new ScreenManager();

        screenManager.addScreen("gs",new GameScreen());
        setScreen(screenManager.getScreen("gs"));
    }

}
