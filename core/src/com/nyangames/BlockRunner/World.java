package com.nyangames.BlockRunner;

import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NyanD on 6/07/2014.
 */
public class World {

    private List<Block> blocks;

    private Player player;

    public World(){
        createDemoWorld();
    }

    private void createDemoWorld(){
        player = new Player(7,2);
        blocks = new ArrayList<Block>();

        /**
        for (int i=0;i<10;i++){
            blocks.add(new Block(i,1));
            blocks.add(new Block(i,0));
            blocks.add(new Block(i,7));
        }
        for (int i=2;i<6;i++) {
            blocks.add(new Block(9, i));
        }

        for (int i=3;i<6;i++){
            blocks.add(new Block(6,i));
        }
         **/
    }



    public List<Block> getBlocks() {
        return blocks;
    }

    public Player getPlayer() {
        return player;
    }
}
