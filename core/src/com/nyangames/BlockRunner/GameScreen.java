package com.nyangames.BlockRunner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.nyangames.BlockRunner.eos.components.*;
import com.nyangames.BlockRunner.eos.entities.Entity;
import com.nyangames.BlockRunner.eos.entities.EntityFactory;
import com.nyangames.BlockRunner.eos.systems.*;
import com.nyangames.BlockRunner.screens.BaseScreen;

import java.util.List;


/**
 * Created by NyanD on 6/07/2014.
 */
public class GameScreen extends BaseScreen{

    private World world;
    private PhysicsSystem system;
    private SystemManager systemManager;
    private EntityFactory entityFactory;

    private SpriteFactory spriteFactory;

    private OrthographicCamera cam;
    private RenderSystem render;

    @Override
    public void update(float delta) {
        List<Entity> temp = entityFactory.getEntities();
        systemManager.update(temp);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        spriteFactory = new SpriteFactory(new SpriteBatch());
        cam = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        render = new RenderSystem(cam);
        Animation anim = spriteFactory.createAnimation("data/megamanRun.png",250,0,50,50,0.07f);

        world = new World();

        system = new PhysicsSystem(cam);
        entityFactory = new EntityFactory();
        systemManager = new SystemManager();
        systemManager.addSystem(render);
        systemManager.addSystem(system);
        InputSystem input = new InputSystem();
        systemManager.addSystem(input);
        //Gdx.input.setInputProcessor(new GestureDetector(input));
        systemManager.addSystem(new TapMovementSystem());
        Gdx.input.setInputProcessor(input);
        Entity e = entityFactory.createEntity();
        PolygonShape shape = new PolygonShape();
        e.addComponent(new TouchTapComponent(e.getId()));
        e.addComponent(new KeyboardComponent(e.getId()));
        Sprite sprite = spriteFactory.createSprite("data/tree.png");

        /**
        texture = new Texture(Gdx.files.internal("data/tree.png"));
        sprite = new Sprite(texture);
        */

        Vector2[] vertices = VectorArrayFactory.create(0,0,0,-40,-22f,-40f,-22f,0);

         shape.set(vertices);

        Entity treeEntity = entityFactory.createEntity();
        treeEntity.addComponent(new BodyComponent(treeEntity.getId(),system.getWorld(), BodyDef.BodyType.StaticBody,50,
                195,shape,1,0,0,(short)0x0000,(short)0x0000));
        treeEntity.addComponent(new SpriteComponent(treeEntity.getId(),sprite));

        e.addComponent(new BodyComponent(e.getId(),system.getWorld(), BodyDef.BodyType.DynamicBody,30,300,shape,1,0,0,
                (short)0x0002,(short)(0xFFFF & ~0x0002)));
        e.addComponent(new AnimationComponent(e.getId(),anim,true,15,1,true,false,true));
        e.addComponent(new CameraFollowComponent(e.getId(),cam));

        sprite = spriteFactory.createSprite("data/grassblocktile.png");

        vertices = VectorArrayFactory.create(0,0,0,-50,-50f,-50f,-50f,0);

        for (int i=0;i<1280;i+=50) {

            shape = new PolygonShape();
            shape.set(vertices);

            Entity b = entityFactory.createEntity();
            b.addComponent(new BodyComponent(b.getId(), system.getWorld(), BodyDef.BodyType.StaticBody, i, 0, shape,
                    1.0f, 0, 0, (short) 0x0004, (short) (0xFFFF & ~0x0004)));
            b.addComponent(new SpriteComponent(b.getId(),sprite));

            b = entityFactory.createEntity();
            b.addComponent(new BodyComponent(b.getId(),system.getWorld(),BodyDef.BodyType.StaticBody,i,50,shape,1.0f,0,
                    0,(short)0x0004,(short)(0xFFFF & ~0x0004)));
            b.addComponent(new SpriteComponent(b.getId(), sprite));

            b = entityFactory.createEntity();
            b.addComponent(new BodyComponent(b.getId(),system.getWorld(),BodyDef.BodyType.StaticBody,i,700,shape,1.0f,0,
                    0,(short)0x0004,(short)(0xFFFF & ~0x0004)));
            b.addComponent(new SpriteComponent(b.getId(),sprite));
        }

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
