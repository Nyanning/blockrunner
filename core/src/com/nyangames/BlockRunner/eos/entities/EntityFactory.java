package com.nyangames.BlockRunner.eos.entities;

import com.badlogic.gdx.Gdx;

import java.util.*;

/**
 * Factory for creating and managing Entities
 */
public class EntityFactory {
    private Map<Integer, Entity> entities;
    private int lastAssignedID;
    private Set<Integer> avaliableID;

    private static final int MAX_ASSIGNABLE_ENTITIES = 5000;

    public EntityFactory(){
        entities = new HashMap<Integer, Entity>();
        lastAssignedID = 0;
        avaliableID = new HashSet<Integer>();

         Gdx.app.log("EntityFactory","adding vals...");
        for (int i=0;i< MAX_ASSIGNABLE_ENTITIES;i++){
            avaliableID.add(i);
            Gdx.app.log("adding vals...",""+i);

        }
    }

    public Entity createEntity(){
        int id = avaliableID.iterator().next();
        Entity entity = new Entity(id);
        entities.put(id, entity);
        avaliableID.remove(id);
        Gdx.app.log("EntityFactory#createEntity","New id: "+id);
        lastAssignedID = id;

        return entity;
    }

    public void removeEntity(int id){
        if (entities.containsKey(id)) {
            entities.remove(id);
            avaliableID.add(id);
            Gdx.app.log("EntityFactory#removeEntity",id+" added back into Set");
        }

    }

    public Entity getEntity(int id){
        if (!entities.containsKey(id)){
            throw new RuntimeException("Entity does not exist");
        }

        return entities.get(id);
    }

    public List<Entity> getEntities(){
        //Gdx.app.log("EntityFactory#getEntities","returning entities list");
        return new ArrayList<Entity>(entities.values());
    }
}
