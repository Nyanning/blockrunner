package com.nyangames.BlockRunner.eos.entities;

import com.nyangames.BlockRunner.eos.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Entity {
    private int id;
    private List<Component> components;

    protected Entity(int id){
        this.id = id;
        components = new ArrayList<Component>();
    }

    public void addComponent(Component component){
        if (!components.contains(component)){
            components.add(component);
        }
    }

    public List<Component> getComponents(){
        return components;
    }

    public int getId(){
        return id;
    }
}
