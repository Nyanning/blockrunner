package com.nyangames.BlockRunner.eos;

import com.nyangames.BlockRunner.eos.components.Component;

/**
 * Created by NyanD on 7/07/2014.
 */
public class Position extends Component {

    private float x, y;

    public Position(int ownerID) {
        super(ownerID);
        x=0;
        y=0;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
