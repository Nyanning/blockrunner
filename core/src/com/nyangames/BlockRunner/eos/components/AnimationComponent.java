package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.graphics.g2d.Animation;

/**
 * Created by NyanD on 9/07/2014.
 */
public class AnimationComponent extends VisibleComponent {
    private Animation animation;
    private float stateTime;
    private float xOffset, yOffset;
    private boolean flipX, flipY;
    private boolean loopAnimation;
    public AnimationComponent(int ownerID, Animation animation, boolean visible, float xOffset, float yOffset, boolean flipX, boolean flipY, boolean loopAnimation) {
        super(ownerID, visible);
        this.animation = animation;
        this.stateTime = 0f;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.flipX = flipX;
        this.flipY = flipY;
        this.loopAnimation = loopAnimation;
    }

    public AnimationComponent(int ownerID, Animation animation){
        this(ownerID, animation, true,0,0, false, false, true);
    }

    public AnimationComponent(int ownerID, Animation animation, float xOffset, float yOffset){
        this(ownerID,animation,true,xOffset,yOffset, false, false, true);
    }

    public AnimationComponent(int ownerID, Animation animation, boolean loopAnimation){
        this(ownerID,animation,true,0,0,false,false,true);
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public float getStateTime() {
        return stateTime;
    }

    public void setStateTime(float stateTime) {
        this.stateTime = stateTime;
    }

    public void addStateTime(float delta){
        this.stateTime += delta;
    }

    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }

    public boolean isFlipX() {
        return flipX;
    }

    public void setFlipX(boolean flipX) {
        this.flipX = flipX;
    }

    public boolean isFlipY() {
        return flipY;
    }

    public void setFlipY(boolean flipY) {
        this.flipY = flipY;
    }

    public boolean isLoopAnimation() {
        return loopAnimation;
    }

    public void setLoopAnimation(boolean loopAnimation) {
        this.loopAnimation = loopAnimation;
    }
}
