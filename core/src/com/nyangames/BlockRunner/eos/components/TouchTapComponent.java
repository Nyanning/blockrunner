/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by NyanD on 14/07/2014.
 */
public class TouchTapComponent extends Component {
    private boolean isTap;
    private Queue<Vector2> tappedQueue;

    public TouchTapComponent(int ownerID) {
        super(ownerID);
        isTap = false;
        tappedQueue = new LinkedList<Vector2>();
    }

    public Queue<Vector2> getTappedQueue() {
        return tappedQueue;
    }

    public boolean isTap() {
        return isTap;
    }

    public void setTap(boolean isTap) {
        this.isTap = isTap;
    }
}
