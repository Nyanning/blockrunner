package com.nyangames.BlockRunner.eos.components;

/**
 * Created by NyanD on 9/07/2014.
 */
public abstract class VisibleComponent extends Component{
    private boolean visible;
    private int zIndex;//used for ordering the textures

    public VisibleComponent(int ownerID, boolean visible) {
        super(ownerID);
        this.visible = visible;
    }

    public VisibleComponent(int ownerID){
        this(ownerID, true);
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
