package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by NyanD on 9/07/2014.
 */
public class SoundEffectComponent extends Component {
    private Sound sound;

    public SoundEffectComponent(int ownerID, Sound sound) {
        super(ownerID);
        this.sound = sound;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

}
