package com.nyangames.BlockRunner.eos.components;

/**
 * Created by NyanD on 7/07/2014.
 */
public abstract class Component {
    private int ownerID;

    public Component(int ownerID){
        this.ownerID = ownerID;
    }

    public int getOwnerID(){
        return ownerID;
    }
}
