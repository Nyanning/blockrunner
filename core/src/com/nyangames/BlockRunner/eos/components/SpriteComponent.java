package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by NyanD on 8/07/2014.
 */
public class SpriteComponent extends VisibleComponent {
    private Sprite curSprite;
    private boolean visible;

    public SpriteComponent(int ownerID, Sprite curSprite) {
        this(ownerID, curSprite, true);
    }

    public SpriteComponent(int ownerID, Sprite curSprite, boolean visible){
        super(ownerID, visible);
        this.curSprite = curSprite;
    }

    public Sprite getCurSprite() {
        return curSprite;
    }

    public void setCurSprite(Sprite curSprite) {
        this.curSprite = curSprite;
    }
}
