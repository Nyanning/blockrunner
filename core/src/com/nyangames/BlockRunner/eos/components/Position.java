package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by NyanD on 7/07/2014.
 */
public class Position {
    private Vector2 position;

    public Position(float x, float y){
        position = new Vector2(x,y);
    }

    public float getX(){
        return position.x;
    }

    public float getY(){
        return position.y;
    }

    public Vector2 get(){
        return new Vector2(position.x,position.y);
    }

    public void set(Vector2 vector){
        position.x = vector.x;
        position.y = vector.y;
    }

    public void setX(float x){
        position.x = x;
    }

    public void setY(float y){
        position.y = y;
    }
}
