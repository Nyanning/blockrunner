package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by NyanD on 7/07/2014.
 */
public class BodyComponent extends Component {
    private float shape, density, friction, restitution;

    BodyDef bodyDef;
    Body body;
    public BodyComponent(int ownerID, World world, BodyDef.BodyType type, float startX, float startY, Shape shape, float density, float friction, float restitution, short mask, short cat){
        super(ownerID);
        bodyDef = new BodyDef();
        bodyDef.type = type;
        bodyDef.position.set(startX,startY);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.filter.categoryBits = cat;
        fixtureDef.filter.groupIndex = mask;
        body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        body.setUserData(this);
    }


    public Body getBody(){
        return body;
    }

}
