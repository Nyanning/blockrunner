package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Created by NyanD on 9/07/2014.
 */
public class CameraFollowComponent extends Component {
    private OrthographicCamera camera;

    public CameraFollowComponent(int ownerID, OrthographicCamera camera) {
        super(ownerID);
        this.camera = camera;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setCamera(OrthographicCamera camera) {
        this.camera = camera;
    }
}
