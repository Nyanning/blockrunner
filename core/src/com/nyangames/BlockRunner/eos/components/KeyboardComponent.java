/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner.eos.components;

import com.badlogic.gdx.Input;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NyanD on 14/07/2014.
 */
public class KeyboardComponent extends Component {
    private HashMap<Integer, Boolean> buttonPressed;

    public KeyboardComponent(int ownerID) {
        super(ownerID);
        buttonPressed = new HashMap<Integer, Boolean>();
    }

    public Map<Integer, Boolean> getButtonPressed() {
        return buttonPressed;
    }
}
