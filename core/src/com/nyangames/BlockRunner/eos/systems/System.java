package com.nyangames.BlockRunner.eos.systems;

import com.nyangames.BlockRunner.eos.entities.Entity;
import com.nyangames.BlockRunner.eos.components.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by NyanD on 7/07/2014.
 */
public abstract class System {

    public abstract void update(List<Entity> entities);

    /**
     *
     * @param type
     * @param entity
     * @return
     */
    protected Component getComponent(Class<?> type, Entity entity){
        for (Component component : entity.getComponents()){
            if (type.isInstance(component)){
                return component;
            }
        }

        throw new RuntimeException("Component of type "+type.getName()+" doesn't exist in entity");
    }

    /**
     *
     * @param e
     * @param type
     * @return
     */
    protected boolean hasComponent(Entity e, Class<?>... type){
        boolean[] hasClassFlags = new boolean[type.length];
        Arrays.fill(hasClassFlags,Boolean.FALSE);

        int i;
        boolean hasAllClassesFlag;

        //Loop through the components
        for (Component c : e.getComponents()){
            hasAllClassesFlag = true;

            //Check if the component is any of the components that we're
            //looking for
            for (i=0;i<hasClassFlags.length;i++){
                if (type[i].isInstance(c)){
                    hasClassFlags[i] = true;
                }
            }

            //AND the flags together to see if we have all compoonents
            //we're looking for. If all the flags are true, it means
            //all the components were found in the Entity, ANDing
            //will make it true
            for (i=0;i<hasClassFlags.length && hasAllClassesFlag == true;i++){
                hasAllClassesFlag &= hasClassFlags[i];
            }

            if (hasAllClassesFlag){
                return true;
            }

        }

        return false;
    }
}
