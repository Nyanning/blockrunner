/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner.eos.systems;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.nyangames.BlockRunner.eos.components.KeyboardComponent;
import com.nyangames.BlockRunner.eos.components.TouchTapComponent;
import com.nyangames.BlockRunner.eos.entities.Entity;

import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Created by NyanD on 14/07/2014.
 */
public class InputSystem extends System implements InputProcessor, GestureDetector.GestureListener{
    private Map<Integer,Boolean> buttonPressed;
    private List<Entity> curEntities;

    @Override
    public void update(List<Entity> entities) {
        curEntities = entities;
    }

    @Override
    public boolean keyDown(int keycode) {
       // buttonPressed.put(keycode,true);

        for (Entity e : curEntities){
            if (hasComponent(e, KeyboardComponent.class)){
                KeyboardComponent keyboardComponent = (KeyboardComponent) getComponent(KeyboardComponent.class, e);
                keyboardComponent.getButtonPressed().put(keycode, true);
            }
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
       // buttonPressed.put(keycode,false);

        for (Entity e : curEntities){
            if (hasComponent(e, KeyboardComponent.class)){
                KeyboardComponent keyboardComponent = (KeyboardComponent) getComponent(KeyboardComponent.class, e);
                keyboardComponent.getButtonPressed().put(keycode, false);
            }
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        for (Entity e: curEntities){
            if (hasComponent(e, TouchTapComponent.class)){
                TouchTapComponent touchTapComponent = (TouchTapComponent)getComponent(TouchTapComponent.class, e);
                Queue<Vector2> vector2Queue = touchTapComponent.getTappedQueue();
                for (int i=0;i<count;i++){
                    vector2Queue.add(new Vector2(x,y));
                }
            }
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }
}
