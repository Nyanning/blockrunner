package com.nyangames.BlockRunner.eos.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.nyangames.BlockRunner.eos.components.*;
import com.nyangames.BlockRunner.eos.entities.Entity;

import java.util.List;

/**
 * Created by NyanD on 8/07/2014.
 */
public class RenderSystem extends System {
    private int portX, portY, portWidth, portHeight;
    private OrthographicCamera cam;
    private SpriteBatch batch;
    private boolean followEntity;
    private int flip;
    public RenderSystem(OrthographicCamera cam, int portX, int portY, int portWidth, int portHeight, boolean follow) {
        this.portX = portX;
        this.portY = portY;
        this.portWidth = portWidth;
        this.portHeight = portHeight;
        this.cam = cam;
        batch = new SpriteBatch();
        this.followEntity = follow;
        flip = -1;
    }

    public RenderSystem(OrthographicCamera cam){
        this(cam, 0, 0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), true);
    }

    @Override
    public void update(List<Entity> entities) {
        //Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClearColor(0.381f,0.67f,0.96f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);//clears the screen

        cam.update();

        if (cam.zoom < 0.5 || cam.zoom > 1){
            flip = -flip;
            cam.zoom = (float)Math.min(Math.max(0.5,cam.zoom),1);
        }
        //cam.zoom += 0.02*flip;
        cam.zoom = 0.5f;

        batch.setProjectionMatrix(cam.combined);
        Gdx.gl.glViewport(portX,portY,portWidth,portHeight);

        batch.begin();

        for (Entity e : entities){
            boolean followFound = false;
            if (hasComponent(e, VisibleComponent.class)){
                if (hasComponent(e,BodyComponent.class)){
                    BodyComponent body = (BodyComponent) getComponent(BodyComponent.class,e);
                    Vector2 pos = body.getBody().getPosition();
                    VisibleComponent vis = (VisibleComponent) getComponent(VisibleComponent.class,e);

                    if (followEntity && hasComponent(e,CameraFollowComponent.class)){
                        CameraFollowComponent follow
                                = (CameraFollowComponent) getComponent(CameraFollowComponent.class,e);
                        if (follow.getCamera() == cam){
                            followFound = true;
                        }
                    }

                    if (vis instanceof SpriteComponent){
                        Sprite spr = ((SpriteComponent) vis).getCurSprite();
                        spr.setPosition(pos.x - spr.getRegionWidth(),pos.y-spr.getRegionHeight());
                        spr.draw(batch);
                    }

                    if (vis instanceof AnimationComponent){
                        AnimationComponent anim = (AnimationComponent) vis;
                        if (vis.isVisible()) {
                            anim.addStateTime(Gdx.graphics.getDeltaTime());
                            TextureRegion frame
                                    = anim.getAnimation().getKeyFrame(anim.getStateTime(), anim.isLoopAnimation());
                            frame.flip(anim.isFlipX() && !frame.isFlipX(),anim.isFlipY() && !frame.isFlipY());
                            batch.draw(frame, pos.x-frame.getRegionWidth()+anim.getxOffset(),
                                    pos.y-frame.getRegionHeight()+anim.getyOffset());
                            if (followEntity && followFound){
                                moveCameraTowards(pos);
                            }
                        }
                    }
                }
            }
        }
        batch.end();
    }

    private void moveCameraTowards(Vector2 vector){
        cam.position.set(vector.x,vector.y,0);
    }

    public int getPortX() {
        return portX;
    }

    public void setPortX(int portX) {
        this.portX = portX;
    }

    public int getPortY() {
        return portY;
    }

    public void setPortY(int portY) {
        this.portY = portY;
    }

    public int getPortWidth() {
        return portWidth;
    }

    public void setPortWidth(int portWidth) {
        this.portWidth = portWidth;
    }

    public int getPortHeight() {
        return portHeight;
    }

    public void setPortHeight(int portHeight) {
        this.portHeight = portHeight;
    }
}
