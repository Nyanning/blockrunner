package com.nyangames.BlockRunner.eos.systems;


import com.badlogic.gdx.Gdx;
import com.nyangames.BlockRunner.eos.entities.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NyanD on 8/07/2014.
 */
public class SystemManager {
    private List<System> systems;

    public SystemManager(){
        systems = new ArrayList<System>();
    }

    public void addSystem(System system){
        if (!systems.contains(system)){
            systems.add(system);
           // Gdx.app.log("SystemManager#addSystem","Added new system");
        }
    }

    public void update(List<Entity> entities){
        //Gdx.app.log("SystemManager#update","updating systems");
        for (System system : systems){
            system.update(entities);
        }
    }
}
