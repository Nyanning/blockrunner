package com.nyangames.BlockRunner.eos.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.nyangames.BlockRunner.eos.entities.Entity;
import com.nyangames.BlockRunner.eos.components.BodyComponent;
import com.nyangames.BlockRunner.eos.components.Component;

import java.util.List;

/**
 * Created by NyanD on 7/07/2014.
 */
public class PhysicsSystem extends System{
    private World world;
    private Box2DDebugRenderer debugRenderer;
    private Camera camera;

    public PhysicsSystem(Camera camera){
        world = new World(new Vector2(0,-200f),true);//Earth gravity world
        debugRenderer = new Box2DDebugRenderer();
        this.camera = camera;
    }

    @Override
    public void update(List<Entity> entities) {
        world.step(1/45f,6,2);
        //debugRenderer.render(world,camera.combined);

        for (Entity e : entities){
            if (hasComponent(e,BodyComponent.class)){
                BodyComponent bodyComponent = (BodyComponent)getComponent(BodyComponent.class,e);//getBodyComponent(e);
                Vector2 vector = bodyComponent.getBody().getPosition();
                //Gdx.app.log("PhysicsSystem : "+((Component)bodyComponent.getBody().getUserData()).getOwnerID(),
                  //      "("+vector.x+","+vector.y+")");
            }
        }
    }

    public World getWorld(){
        return world;
    }
}
