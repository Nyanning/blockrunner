/*
 * Copyright (c) Robert Duong 2014.
 */

package com.nyangames.BlockRunner.eos.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.nyangames.BlockRunner.eos.components.BodyComponent;
import com.nyangames.BlockRunner.eos.components.KeyboardComponent;
import com.nyangames.BlockRunner.eos.components.TouchTapComponent;
import com.nyangames.BlockRunner.eos.entities.Entity;

import java.util.List;
import java.util.Queue;

/**
 * Created by NyanD on 14/07/2014.
 */
public class TapMovementSystem extends System{
    @Override
    public void update(List<Entity> entities) {
        for (Entity e : entities){
            /**if (hasComponent(e, BodyComponent.class) && hasComponent(e, TouchTapComponent.class)){
                BodyComponent bodyComponent = (BodyComponent) getComponent(BodyComponent.class, e);
                Queue<Vector2> vector2Queue = ((TouchTapComponent)getComponent(TouchTapComponent.class,
                        e)).getTappedQueue();
                Body body = bodyComponent.getBody();
                for (int i=0;i<vector2Queue.size();i++){
                    vector2Queue.remove();
                    body.applyLinearImpulse(0.5f,0,body.getPosition().x,body.getPosition().y,true);
                }

            }
        }*/

            if (hasComponent(e, BodyComponent.class) && hasComponent(e, KeyboardComponent.class)){
                BodyComponent bodyComponent = (BodyComponent) getComponent(BodyComponent.class, e);
                KeyboardComponent keyboardComponent = (KeyboardComponent)getComponent(KeyboardComponent.class, e);
                Body body = bodyComponent.getBody();
                if (keyboardComponent.getButtonPressed().get(Input.Keys.RIGHT) != null && keyboardComponent
                        .getButtonPressed().get(Input.Keys.RIGHT)) {

                    //body.applyLinearImpulse(50f,0,body.getPosition().x,body.getPosition().y,true);
                    //body.setLinearVelocity(body.getLinearVelocity().x + 50, body.getLinearVelocity().y);
                    Gdx.app.log("TapMoveSystem: ", "Force vector applied");
                }

                if (keyboardComponent.getButtonPressed().get(Input.Keys.LEFT) != null && keyboardComponent
                        .getButtonPressed().get(Input.Keys.LEFT)){
                   // body.setLinearVelocity(body.getLinearVelocity().x-50,body.getLinearVelocity().y);
                }

                if (keyboardComponent.getButtonPressed().get(Input.Keys.UP) != null && keyboardComponent
                        .getButtonPressed().get(Input.Keys.UP)){
                   body.applyForceToCenter(0,5000f,true);
                    Gdx.app.log("TapMoveSystem: ","Jump force");
                   // body.setLinearVelocity(body.getLinearVelocity().x,body.getLinearVelocity().y+50);
                }

            }
        }
    }
}
