package com.nyangames.BlockRunner;

/**
 * Created by NyanD on 6/07/2014.
 */
public enum State {
    IDLE, WALKING, JUMPING, DYING;
}
