package com.nyangames.BlockRunner;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by NyanD on 6/07/2014.
 */
public class Block {

    static final float SIZE = 1f;

    private Vector2 position;
    private Rectangle bounds;

    public Block(float x, float y){
        position = new Vector2(x,y);
        bounds = new Rectangle();

        bounds.setHeight(SIZE);
        bounds.setWidth(SIZE);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }
}
